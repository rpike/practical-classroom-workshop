---
title: Magic 8-ball Assignment
nav_order: 1
parent: Workshop Instructions 
---

# GitLab Classroom Set up 

Pre-requisites:
- You must have a GitLab.com account set up 
- You must have GitPod enabled
- {note to self - we may need to enable runners here with cc}

## Steps

### [Create a Group](https://docs.gitlab.com/ee/user/group/manage.html#create-a-group)
1. From the `main menu` go to `Groups`. 
1. Click `view all groups` and then click on `New group` int he top right to create a group, then Create group. 
1. Name the Group `Example Class`. Choose the following settings:
    1. Visibility Level: Private
    1. Role: Software Developer
    1. Who will be using the Group: My company or team
    1. For What: I want to store my code
    1. Do not invite members at this time. 
    1. Create Group

### [Create a Project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project)
1. Click `Create a new project` 
1. Select `Create blank project`
1. Name the project `Module 1`
1. Accept the defaults and click `Create Project`

Note: A readme file is automatically created with some basic instructions on how to use a GitLab project. 

### [Create an issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-an-issue) for the assignment
1. Click on Issues
1. Click the blue `New Issue` button
1. Add a title `Module 1: Magic 8 Ball Python Assignment `
1. Copy the markdown in [this file](https://gitlab.com/devops-education/workshops/practical-classroom-workshop/-/edit/main/issue_text.md). These are the instructions a student would work through. 
<!-- Need to put this into markdown for them to copy into their issue -->

### Complete the assignment
1. Work through the [example issue](https://gitlab.com/devops-education/workshops/practical-classroom-workshop/-/issues/1) as if you were a student in your class. 


[Example Magic 8 Ball Python code](https://gitlab.com/devops-education/workshops/practical-classroom-workshop/-/blob/main/sample_code.txt)
