## Overview 

In this assignment, you will learn how to create a Magic 8 Ball game in Python. The learning outcomes are:

- Create a python file in a GitLab repository
- Compose a basic python script for a Magic 8 Ball Game in your python file
- Use Git to commit the code to your repository 
- Connect your repository to GitPod
- Run the Magic 8 Ball ame
- Modify the code 
- Use Git to commit the code to your repository in both GitPod and the WebIDE
- Share your code with a classmate 
- Run a classmate's code 


##  Create a python file in a GitLab repository
1. From your GitLab project, click the `Repository` option in the left sidebar. 
1. At the top click on the + sign to add a new file to the repository.
1. Name the file `magic_8ball.py`, ignore the template drop down, and commit the changes.
1. You should now be able to see the new file in the repository. 

##  Compose a basic python script for a Magic 8 Ball Game in your python file and make a Commit 
1. Review the sample Magic 8 ball python code [here](https://gitlab.com/devops-education/workshops/practical-classroom-workshop/-/blob/main/sample_code.txt). 
1. Copy the code.
1. Click the magic_8ball.py file then click the blue `Open in Web IDE` button
1. Paste the code into the file.
1. Click the blue `Create Commit` button on the bottom left. 
1. Input a commit message of your choosing or keep the default.
1. Select the radio button for `commit to main branch`
1. Click blue `Commit` button. 
1. After successfully commiting, you are still in the Web IDE and your hanges have been saved. 
1. Click `Module 1` on the top left to return to the project. 
1. Your new code is now visisble in the `magic_8ball.py` file in the repository. Click the file to view your changes.

## Connect your repository to GitPod 
Now we're going to open it in GitPod.
1. From the file in the repository, click the drop down menu next to the  `Open in WebIDE` blue button. Change it to `Open in GitPod` and click the `Open in GitPod button.`
1. GitPod will open, click continue with GitLab 
1. Choose the editor of your preference for opening workspaces - for this example we'll use VS Code Browser.
1. VS Code will open in your browser inside GitPod.
1. Install the recommended extensions for Python by clicking install.
1. The Python extension for Visual Studio Code will install.
1. You can close the Get Started Tabs up top and any release notes tabs.
1. The only tab open should be magic_8ball.py 

## Run the Magic 8 Ball python code in GitPod
1. Clcik on Terminal under the code. It's near the middle of the page. 
1. Run the magic_8ball.py code in your GitPod browser by typing `python3 magic_8ball.py`
1. Play the Magic 8 Ball game by answering it's questions. 
1. When you're done type no. The program will exit.


## Modify the code and commit the code to a new branch
1. Next, you'll create a new branch to commit your changes. Click on `main` in the bottom left of the window; this will open a menu up top. Click `create new branch`. Name it `python-example-initials`. Put your initials in place of the word initials. Hit Enter. You should see the new branch name in the bottom where `Main` was before.
1. Change one of the 8-ball answers in line 5.
1. Save the file. (Hamburger menuu top left, File, Save.)
1. Click the source control tab (denoted by the git symbol, three symbols down), click the + sign next to the file name to stage the changes. 
<!-- needs image-->
1. Type a commit message noting the changes you made in the text box above the `commit` button, and click the `commit` button. 
1. Send the changes to the repo by clicking the `publish branch` or `sync changes` button. The changes will be sent to the repository. 
1. Go back to your project on GitLab.com, you'll see a banner with the option to [create a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html). Click the button and fill out the Merge request. You can accept most of the defaults. Input a message into the deascription. 
1. Click `Create Merge request`
1. In the MR, scroll down to the `ready to merge` message.
1. Click the `Merge` button on the MR to merge the changes from your branch into the main branch. 
1. Go Back to GitPod. Click `python-example-initials` on the bottom. Select `Main` from the pop up menu. 
1. Click the Source Control tab, and up top, click the three button menu to the right of the refresh icon.
1. Click `pull` to bring the changes from GitLab into GitPod. 
1. Your main branch in GitPod is now concurrent. 


## Share your code with a classmate and make an MR to a classmate's python file. 
1. Go back to GitLab.com, from your Example Class\Module 1 project, hover over Project Information and click Members. Click the blue `Invite members` button and invite one of your classmates to the Project with the Developer Role. [Learn more about roles](https://docs.gitlab.com/ee/user/permissions.html). Use their email to invite them. 
1. You should also have been added to a classmate's Project. The Group and Project will appear in your lists of Projects.
1. Navigate to your classmate's Project. 
1. Open the WebIDE in your classmate's project. The button should be around the middle right of the page below the file type color line. Click the `magic_8ball.py` file.
1. Edit one of the magic 8 ball answers. 
1. Use the WebIDE to Commit the change like before. You cannot commit to main with `developer access`. Create a new branch and accept the defaults. Type a commit message, make sure `start a new merge request` is selected, and commit the change. 
1. You are now in the Merge Request. Fill out the Merge Request with some details. 
1. Assign it back to your classmate using the drop down menu under the description.
1. Click `create merge request`.
1. You should have a merge request assigned to you as well. An icon on the top right of the page should have a yellow/orange number attached to it. Click it and click `assigned to you`. 
1. Open the merge request by clicking the title of the merge request. 
1. You can view the changes by clicking the `changes` tab up top. Changes made are in green.
1. You can decide to merge the changes into main in the `overview` tab. 

## Artifacts
- [ ] Link both MRs to this issue
- [ ] Add a screen shot of the results of the Magic 8 ball game as a comment in the issue.
