---
title: Home
nav_order: 1
description: "Practical Uses of GitLab in the Classroom "
permalink: /
---

# Practical Uses of GitLab in the Classroom 

Welcome to the GitLab Practical Uses of GitLab in the Classroom Workshop.

## Overview 
This introductory workshop is for faculty members seeking to understand how to introduce DevOps and simple DevOps workflows into their classroom with GitLab. This workshop will be of interest to faculty who teach Programming and Computer Science, Application Design and Development, Project Planning, and/or simply use code in any part of their course, especially if teaching these courses requires teamwork or collaboration from the students. 

## Learning Outcomes
By the conclusion of the workshop, participants will be able to:
* Describe the evolution of software development from Waterfall to Agile to DevOps 
* Describe how GitLab the platform relates to DevOps
* Identify connections between DevOps workflows and standard classroom workflows
* Structure a typical classroom in GitLab
* Employ basic GitLab workflows in an example classroom
* Identify how GitLab can be integrated into curriculum 


# Workshop Outline
1. 
2. 
3. 
4. 
5. 

[Next Page](https://devops-education.gitlab.io//workshops/practical-classroom-workshop/course/overview/)
